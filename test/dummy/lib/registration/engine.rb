module Registration
  class Engine < ::Rails::Engine
    isolate_namespace Registration
  end
end
